import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

// Importación de idiomas
import '@angular/common/locales/global/es';
import '@angular/common/locales/global/fr';
 
import { AppComponent } from './app.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { SecuredomPipe } from './pipes/securedom.pipe';
import { ShowhidePipe } from './pipes/showhide.pipe';
 
@NgModule({
  declarations: [
    AppComponent,
    CapitalizePipe,
    SecuredomPipe,
    ShowhidePipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule { }