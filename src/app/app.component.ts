import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pipes-handbook';

  name1 = 'Cameron Howe';
  name2 = 'Gordon Clark';
  name3 = 'Donna Clark';
  name4 = 'Joe MacMillan';
  name5 = 'John Bosworth';
  name6 = 'joE mAcMillaN';

  myArray = ['Cameron', 'Gordon', 'Donna', 
             'Joe', 'John', 'Sarah', 'Ryan', 
             'Joanie', 'Nathan', 'Katie', 'Diane'];

  PI: number = Math.PI;
  percent = 0.2345;
  salary = 123456;

  character = {
    name: 'Cameron',
    ability: 'Programmer',
    age: 38,
    adress: {
      street: 'Carlos III',
      number: 39
    }
  }

  promiseValue = new Promise<string>( (resolve) => {
    setTimeout(() => {
      resolve('Data arrived');
    }, 4500);
  });

  date = new Date();
  language = 'es';
  videoUrl = 'https://www.youtube.com/embed/53KnQ2sNyu8';
  activate= false;
}

