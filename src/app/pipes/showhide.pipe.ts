import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showhide'
})
export class ShowhidePipe implements PipeTransform {

  transform(value: string, show: boolean = false ): string {

    return ( show ) ? '*'.repeat(value.length) : value;
  }

}
